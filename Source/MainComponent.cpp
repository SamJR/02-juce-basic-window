/*
  ==============================================================================

    This file was auto-generated!

  ==============================================================================
*/

#include "MainComponent.h"


//==============================================================================
MainComponent::MainComponent()
{
    setSize (500, 400);
    
    
    addAndMakeVisible(colourSelector1);
}

MainComponent::~MainComponent()
{

}

void MainComponent::resized()
{
    //Display Window Size
    DBG ("Width: " << getWidth() << "\nHeight: " << getHeight() << "\n");
    
    colourSelector1.setBounds (10, 10, getWidth() - 20, getHeight() * 0.5);
}


void MainComponent::paint(Graphics &g)
{
    DBG ("Call to paint function " << x << " " << y); //print mouse coords that are stored in x and y variable
    g.setColour(colourSelector1.getCurrentColour());
    g.drawEllipse (x-25, y-25, 50, 50, 1); //minus half the diameter to centre circle on mouse click
    
}

void MainComponent::mouseUp (const MouseEvent& event)
{
    DBG("Mouse Released: X = " << event.x << ", Y = " << event.y << "\n"); //print mouse coords direct from mouse event object
    x = event.x; //assign x coordinate of mouse click to variable x
    y = event.y; //assign y coordinate of mouse click to variable y
    repaint(); //call the paint function
}



