//
//  myButton.cpp
//  JuceBasicWindow
//
//  Created by Samuel Reis on 24/10/2016.
//
//

#include "myButton.hpp"

myButton::myButton()
{
    myButton.setBounds(0,0,getWidth(), gethHeight());
}

myButton::~myButton()
{
    
}


void myButton::paint(Graphics &g)
{
   
    g.fillRoundedRectangle (20, 20, 20, 20, 20);
}

void myButton::mouseExit (const MouseEvent& event)
{

    repaint(); //call the paint function
}

void myButton::mouseEnter (const MouseEvent& event)
{
    
    repaint(); //call the paint function
}

void myButton::mouseUp (const MouseEvent& event)
{
    
    repaint(); //call the paint function
}

void myButton::mouseDown (const MouseEvent& event)
{
    
    repaint(); //call the paint function
}