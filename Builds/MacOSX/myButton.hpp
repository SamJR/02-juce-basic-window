//
//  myButton.hpp
//  JuceBasicWindow
//
//  Created by Samuel Reis on 24/10/2016.
//
//

#ifndef myButton_hpp
#define myButton_hpp

#include <stdio.h>
#include "../JuceLibraryCode/JuceHeader.h"



class myButton: public Component
{
public:
    myButton();
    ~myButton();
    
    virtual void mouseEnter (const MouseEvent& event) override;
    virtual void mouseExit (const MouseEvent& event) override;
    virtual void mouseDown (const MouseEvent& event) override;
    virtual void mouseUp (const MouseEvent& event) override;
    void paint (Graphics &g) override;
    
private:
    
};

#endif /* myButton_hpp */